package csse.virtualrealitysimulator;

import java.util.Scanner;

import csse.virtualrealitysimulator.datagloves.P5;

public class VirtualRealitySimulatorDemo {

	TextMonitor tm;
	DataGlove dg;
	
	public VirtualRealitySimulatorDemo() {
		tm = new TextMonitor();
		dg = new P5();
		dg.attachObserver(tm);
		dg.setsimulateConnectionErrorFlag(false);
	}

	
	public static void main(String[] args) {
		VirtualRealitySimulatorDemo vrsd = new VirtualRealitySimulatorDemo();
		vrsd.demo();

	}
	
	public void demo() {
		System.out.println("Enter the number of samples measured ");
		Scanner kbd = new Scanner(System.in);
		int tracking_attempts = kbd.nextInt();
		kbd.close();
		dg.start(tracking_attempts);
		dg.stop();
	}
	

}

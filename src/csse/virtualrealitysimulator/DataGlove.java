package csse.virtualrealitysimulator;

import java.util.ArrayList;
import java.util.List;

import csse.virtualrealitysimulator.exceptions.DeviceConnectionException;
import csse.virtualrealitysimulator.utilities.RandomNumbers;

public abstract class DataGlove implements Observable {
	
	

	List<Observer> observers = new ArrayList<Observer>();
	
	@Override
	public void attachObserver(Observer o) {
		this.observers.add(o);
		
	}
	@Override
	public void detachObserver(Observer o) {
		this.observers.remove(o);
		
	}
	@Override
	public void notifyObservers(Observable observable) {
		for (Observer observer : observers) {
			observer.update(observable);
		}

		
	}
	
	// glove properties
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getResolution() {
		return resolution;
	}
	public void setResolution(int resolution) {
		this.resolution = resolution;
	}
	
	public String getSensor_type() {
		return sensor_type;
	}
	public void setSensor_type(String sensor_type) {
		this.sensor_type = sensor_type;
	}
	public double getSample_rate() {
		return sample_rate;
	}
	public void setSample_rate(double sample_rate) {
		this.sample_rate = sample_rate;
	}
	
	//glove data
	public double getPitch() {
		return pitch;
	}
	public void setPitch(double pitch) {
		this.pitch = pitch;
	}
	public double getYaw() {
		return yaw;
	}
	public void setYaw(double yaw) {
		this.yaw = yaw;
	}
	public double getRoll() {
		return roll;
	}
	public void setRoll(double roll) {
		this.roll = roll;
	}
	public boolean[][] getFingersBended() {
		return fingers_bended;
	}
	public void setFingersBended(boolean[][] fingers) {
		this.fingers_bended = fingers;
	}
		
	//private and protected members
	protected boolean connection_error; //glove status
	public boolean simulateConnectionError() {
		return connection_error;
	}
	public void setsimulateConnectionErrorFlag(boolean connected) {
		this.connection_error = connected;
	}

	protected String name;
	protected int resolution; //in bits
	protected String sensor_type ;
	protected double sample_rate ; //in Khz
	
	// index 0 x; 1 y; 2 z
	private double location[];
	
	public double[] getLocation() {
		return location;
	}
	public void setLocation(double[] location) {
		this.location = location;
	}
	//rotation
	private double pitch, yaw, roll;
	/*
	 * bend fingers first index indicates hand right(0) to left (1), second index
	 * indicate fingers from thumb (0) to pinky(4)
	 */
	
	private boolean fingers_bended[][] ;
	
	
	public int start(int number_of_samples) {
		
		int returnCode =0;
		
		try {
			if (this.connect()==0){
				System.out.println("Device succesfully connected");
				System.out.println("Device start sampling "+ number_of_samples + " times");
				for (int i = 1; i <= number_of_samples; i++) {			
					collectData();
					notifyObservers(this);
				}
				
			}
		} catch (DeviceConnectionException e) {
			e.printStackTrace();
			returnCode=-1;
		}
		
		
		return returnCode;
	}
	public int stop() {
		int returnCode =0;

		try {
			if (this.disconnect()==0){
				System.out.println("Device succesfully disconnected");
				System.out.println("Device succesfully stopped");
			}
		} catch (DeviceConnectionException e) {
			System.out.println("Warning: Could not disconnect device");
			returnCode=-1;
		}


		return returnCode;
	}
	
	protected abstract int connect() throws DeviceConnectionException;
	protected abstract int disconnect() throws DeviceConnectionException;
	private void collectData() {
		location = new double[3];
		fingers_bended = new boolean[5][2];
		for (int i =0;i<location.length;i++) {
			location[i] = RandomNumbers.generateRandomNumber(511, -512);
		}
		
		pitch = RandomNumbers.generateRandomNumber(511, -512);
		yaw = RandomNumbers.generateRandomNumber(511, -512);
		roll =  RandomNumbers.generateRandomNumber(511, -512);
		
		for (int j =0; j<2; j++) {
			for (int i= 0; i<5;i++) {
				fingers_bended[i][j]= RandomNumbers.generateRandomBooleanValue();
			}
		}
		
	}
	

}

package csse.virtualrealitysimulator;

import csse.virtualrealitysimulator.utilities.RandomNumbers;

public class TextMonitor implements Observer {

	@Override
	public void update(Observable o) {
		System.out.println("***************************************************");
		System.out.println("data glove's name:" + ((DataGlove) o).getName());
		System.out.println("data glove's sensor type:" + ((DataGlove) o).getSensor_type());
		System.out.println("data glove's resolution:" + ((DataGlove) o).getResolution());
		System.out.println("data glove's location: x=" + 
		((DataGlove) o).getLocation()[0] +
		" location: y=" + ((DataGlove) o).getLocation()[1] +
		" location: z=" + ((DataGlove) o).getLocation()[2]);
		System.out.println("data glove's pitch, yaw and roll :" +
		((DataGlove) o).getPitch() + 
		", " + ((DataGlove) o).getYaw() + "," + 
		((DataGlove) o).getRoll() );
		getFingersBended(((DataGlove) o).getFingersBended());
		System.out.println("***************************************************");
		
	}
	
	private void getFingersBended (boolean fingers_bended[][]) {
		for (int j =0; j<2; j++) {
			System.out.println("Hand " + j + ":");
			for (int i= 0; i<5;i++) {
				if (fingers_bended[i][j])
					System.out.println("Finger " + i + " bended");
			}
		}
		
	}
	

}

package csse.virtualrealitysimulator;

public interface Observer {
	public void update(Observable o);
}

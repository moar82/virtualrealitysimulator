package csse.virtualrealitysimulator.utilities;

import java.util.Random;

public final class RandomNumbers {
	
	private RandomNumbers() {}

	public static double generateRandomNumber (double max, double min) {
		
		return (Math.random()* ( (max-min) +1)) + min;
	}
	
public static boolean generateRandomBooleanValue () {
		
	Random rd = new Random(); // creating Random object;	
		return rd.nextBoolean();
	}

public static int generateRandomReturnValue() {
	Random rd = new Random();
	return rd.nextInt(255);
}

	
}

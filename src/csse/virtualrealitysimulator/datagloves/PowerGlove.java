package csse.virtualrealitysimulator.datagloves;

import csse.virtualrealitysimulator.DataGlove;
import csse.virtualrealitysimulator.exceptions.DeviceConnectionException;
import csse.virtualrealitysimulator.utilities.RandomNumbers;

/**
 * @author Rodrigo Morales
 *
 */
public class PowerGlove extends DataGlove {

	public  PowerGlove() {
		name = "Power Glove";
		resolution = 8;//bits
	    sensor_type = "Ultrasonic";
	    sample_rate = 0;//40 KHz
	}
	
	
	/**
	 *This method is simplified for learning purposes.  The print statement,
	 *represents all the effort required to established a connection using serial port.
	 *In real life, you will need to add the functionality to communicate to an API,
	 *typically a JAR file that embeds the logic to communicate with the hardware
	 */
	@Override
	protected int connect() throws DeviceConnectionException {
		int rv = 0;
			if (connection_error) {
				throw new DeviceConnectionException("Could not established connection through serial port");
			}
			else {
				System.out.println("Connection through serial port succesfully established");
			}
		return rv;
	}

	/**
	 *This method is simplified for learning purposes.  The print statement,
	 *represents all the effort required to close a connection using serial port.
	 *In real life, you will need to add the functionality to communicate to an API,
	 *typically a JAR file that embeds the logic to communicate with the hardware
	 */
	@Override
	protected int disconnect() throws DeviceConnectionException {
		int rv = 0;
		if (RandomNumbers.generateRandomBooleanValue()==false) {
			throw new DeviceConnectionException("Could not disconnect from serial port");	
		}
		else {
			System.out.println("Serial connection succesfully closed");
		}
	return rv;
	}

}

package csse.virtualrealitysimulator.datagloves;

import csse.virtualrealitysimulator.DataGlove;
import csse.virtualrealitysimulator.exceptions.DeviceConnectionException;
import csse.virtualrealitysimulator.utilities.RandomNumbers;

public class P5 extends DataGlove {


	public P5() {
		name = "P5";
		resolution =0;//unknown
	    sensor_type = "Infrared";
	    sample_rate = 45; //45 Khz
	}

	/**
	 *This method is simplified for learning purposes.  The print statement,
	 *represents all the effort required to established a connection using USB port.
	 *In real life, you will need to add the functionality to communicate to an API,
	 *typically a JAR file that embeds the logic to communicate with the hardware
	 */
	@Override
	protected int connect() throws DeviceConnectionException {
		int rv = 0;
			if (connection_error) {
				throw new DeviceConnectionException("Could not established connection through USB port");
			}
			else {
				System.out.println("Connection through USB port succesfully established");
			}
		return rv;
	}

	/**
	 *This method is simplified for learning purposes.  The print statement,
	 *represents all the effort required to close a connection using USB.
	 *In real life, you will need to add the functionality to communicate to an API,
	 *typically a JAR file that embeds the logic to communicate with the hardware
	 */
	@Override
	protected int disconnect() throws DeviceConnectionException {
		int rv = 0;
		if (RandomNumbers.generateRandomBooleanValue()==false) {
			throw new DeviceConnectionException("Could not disconnect from USB port");	
		}
		else {
			System.out.println("USB connection succesfully closed");
		}
	return rv;
	}

}

package csse.virtualrealitysimulator.exceptions;

public class DeviceConnectionException extends Exception {
    public DeviceConnectionException(String message) {
        super(message);
    }

}
